# JavaScript Objects #

https://www.w3schools.com/js/js_objects.asp


In JavaScript, almost "everything" is an object.

Any function in JavaScript can be used to create custom object classes, simply by calling it using the keyword new. When called in this way, the special variable this inside the function references the new object that is being constructed (it normally refers to the 'current' object, which is usually window, except inside methods)